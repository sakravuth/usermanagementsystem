<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserManagementSystemController@index')->middleware(["auth.admin","auth"]);//->middleware("auth.basic");
Route::get('/create', 'UserManagementSystemController@create');
Route::post('/add', 'UserManagementSystemController@save_data');
Route::get('/information', 'UserManagementSystemController@show_data')->name('information')->middleware(["auth.admin","auth"]);
Route::get('/view/article/{id}', 'UserManagementSystemController@view')->name('view');
Route::get('/view/{id}', 'UserManagementSystemController@update')->name('article.view');
Route::put('/edit/{id}', 'UserManagementSystemController@edit')->name('article.edit');
Route::get('/delete/{id}', 'UserManagementSystemController@delete')->name('delete-article');
Route::get('/search', 'UserManagementSystemController@search');

Route::get('/khmer', 'UserManagementSystemController@khmer');
Route::get('/english', 'UserManagementSystemController@english');



Route::get('/sendMail', 'MailController@sendMail');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/getuser',function(){
    echo 'Hello '.Auth::User()->name;
});
Route::get('/update',function(Request $request){
    echo $request->user()->name;
});

Route::get('/logout', 'HomeController@logout');
Route::get('/logoutall', 'HomeController@logoutall');
Route::post('/logoutAction', 'HomeController@logoutAction');


Route::get('/customlogin', "CustomLoginController@loginForm");
Route::post('/customloginaction', "CustomLoginController@loginAction");

