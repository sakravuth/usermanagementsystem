<?php

use Illuminate\Database\Seeder;
use App\User;

class UserManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory("App\UserModel", 100)->create();
    }
}
