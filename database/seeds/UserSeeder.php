<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table("role_user")->truncate();
        $admin = User::create([
            "name"=>"admin",
            "email"=>"admin@admin",
            "password"=>bcrypt("admin@admin")
        ]);

        $user=User::create([
            "name"=>"vuth",
            "email"=>"vuth@vuth",
            "password"=>bcrypt("vuth@vuth")
        ]);

        $adminRole = Role::where("name","admin")->first();
        $userRole = Role::where("name","user")->first();
        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);

    }
}
