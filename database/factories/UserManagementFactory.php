<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserModel;
use Faker\Generator as Faker;

$factory->define(UserModel::class, function (Faker $faker) {
    return [
        "name"=>$faker->name,
        "email"=>$faker->unique()->safeEmail,
        "phone_number"=>$faker->phoneNumber,
        "profile"=>$faker->image('upload',200,200, null, true)
    ];
});
