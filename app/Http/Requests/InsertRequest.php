<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>"required|max:5",
            "email"=>"required|unique:user_management",
            "phone_number"=>"required|unique:user_management", 
            "profile"=>"nullable",
        ];
    }
    public function messages(){
        return [
            "name.required" => __("message.name_required"),
            "name.max" => __("message.name_max"),
            "email.required" => __("message.email_require"),
            "email.unique" => __("message.email_unique"),
            "phone_number.required" => __("message.phone_required"),
            "phone_number.unique" => __("message.phone_unique"),
        ];
    }
}
