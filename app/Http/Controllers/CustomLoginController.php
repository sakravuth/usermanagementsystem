<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;

class CustomLoginController extends Controller
{
    function loginForm(){
        return view('layouts/customlogin');
    }

    function loginAction(Request $request){
        $credential = [
            "email" => $request->email,
            "password" => $request->password,
        ];
        if(\Auth::attempt($credential)){
            return redirect('/');
        }
    }
}
