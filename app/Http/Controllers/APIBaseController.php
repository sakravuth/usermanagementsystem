<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIBaseController extends Controller
{
    public function sendResponse($data, $message, $status){
        $response = [
            "status" => $status,
            "message" => $message,
        ];
        if($status){
            $response['data'] = $data;
        }
        return response()->json($response, 200);
    }

    public function sendResponseAfterDelete($message, $status){
        $response = [
            "status" => $status,
            "message" => $message,
        ];
        return response()->json($response, 200);
    }
}
