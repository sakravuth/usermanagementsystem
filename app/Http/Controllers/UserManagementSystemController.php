<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use App\Http\Requests\InsertRequest;
use Session;

class UserManagementSystemController extends Controller
{
    public function index(){
        return view('index');
    }
    public function create(){
        return view('layouts.add-user');
    }
    public function save_data(InsertRequest $request){
        
        //$this->validate($request,[
        //    "name"=>"required|max:5",
        //    "email"=>"required|unique:user_management",
        //    "phone_number"=>"required",   
        //]);
        //$request->validate([
            //style two
        //]);
        $user = new UserModel();
        $user->fill($request->all());

        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $destination = "public/upload";
            $fileName= $files->store($destination);
            $user->profile = str_replace('public', '', $fileName);        
        }else{
            $destination = "upload\profile.jpg";
            $user->profile = $destination;
        }
        
        $user->save();
        return redirect('/information');
        
  
    }
    public function show_data(){
        $startdate = '';
        $enddate = '';
        $filters=['id','name','email','phone_number'];
        $users = UserModel::orderby('id', 'desc')->paginate(10);
        $count = UserModel::orderby('id', 'desc')->paginate(10)->count();
        return view('layouts.information')->with('users', $users)
                                          //->with('search', $search)    
                                          ->with('filters', $filters);
    }
    public function view($id){
        $users = UserModel::find($id);
        return view('layouts.view_form')->with('users', $users);
    }
    public function update($id){
        $user = UserModel::find($id);
        return view('layouts.edit_form')->with('user', $user);
    }
    public function edit($id, Request $request){
         $user = UserModel::find($id);
         $user->update($request->all()); 
         if($request->hasFile('profile')){
            $files = $request->file('profile');
            $destination = "public/upload";
            $fileName= $files->store($destination);
            $user->profile = str_replace('public', '', $fileName);
         }
         $user->save();
         return redirect('/information');
            
    }
    public function delete($id){
        UserModel::destroy($id);
        return redirect('/information');
    }
    public function search(Request $request){
        $startdate = $request->startdate;
        $enddate = $request->enddate;
        $search = $request->search;
        $option = $request->name;
        $filter = $request->filter;
        $filters=['id','name','email','phone'];
        $count = UserModel::orderby('id', 'desc')->paginate(10)->count();
        $result = UserModel::where($option,'LIKE','%'.$search.'%')
                      ->orwhere('created_at','<',[$startdate,$enddate])
                      ->paginate(10);
        return view('layouts.information')->with('users', $result)
                                          ->with('filters', $filters)
                                          ->with('search', $search)
                                          ->with('startdate', $startdate)
                                          ->with('enddate', $enddate)
                                          ->with('count', $count);
    }

    public function uploadAction(Request $request){
        $files = $request->file('thumbnail');
        $destination = "public/upload";
        foreach ($files as $file){
            $file->store($destination);
        }
    }

    public function khmer(){
        \App::setlocale("kh");
        \Session::put("locale","kh");
        return redirect()->back();
    }
    public function english(){
        \App::setlocale("en");
        \Session::put("locale","en");
        return redirect()->back();
    }

}
