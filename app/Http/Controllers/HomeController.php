<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/information');
        //return view('home');
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
    public function logoutall(){
        return view('layouts.logout-user');
    }
    public function logoutAction(Request $request){
        $user = new UserModel;
        $user->password = $request->password;
       //echo dd($request);
        Auth::logoutOtherDevices($user);
        return redirect('/login');
    }
}
