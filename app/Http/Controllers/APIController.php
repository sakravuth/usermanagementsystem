<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
class APIController extends APIBaseController
{
    public function index(){
        $users = UserModel::all();
        if(count($users) == 0){
            return $this->sendResponse($users, "No records!", false);
        }else{
            return $this->sendResponse($users, "Get data successfully!", true);
        }
        
    }

    public function insert(Request $request){
        $user = new UserModel();
        $user->fill($request->all());

        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $destination = "public/upload";
            $fileName= $files->store($destination);
            $user->profile = str_replace('public', '', $fileName);        
        }else{
            $destination = "upload\profile.jpg";
            $user->profile = $destination;
        }
        
        $user->save();
        return $this->sendResponse($user, "User Insert Successfully", true);
    }

    public function delete($id){
        UserModel::destroy($id);
        return $this->sendResponseAfterDelete("Delete User Successfully", true);
    }

    public function update($id){
        $user = UserModel::find($id);
        return $this->sendResponse($user, "Get data successfully!", true);
    }

    public function edit($id, Request $request){
        $user = UserModel::find($id);
        $user->update($request->all()); 
        if($request->hasFile('profile')){
           $files = $request->file('profile');
           $destination = "public/upload";
           $fileName= $files->store($destination);
           $user->profile = str_replace('public', '', $fileName);
        }
        $user->save();
        return $this->sendResponse($user, "Update data successfully!", true);        
   }
   public function ajax_index(){
       return view('layouts/ajax-index');
   }

}
