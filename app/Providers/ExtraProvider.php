<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ExtraProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::if("hasrole", function($expression){
            if(\Auth::user()){
                if(\Auth::user()->hasAnyRoles($expression)){
                    return true;
                }
            }
            return false;
        });
    }
}
