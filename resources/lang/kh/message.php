<?php
    return [
        'UserManagementSystem' => 'ប្រព័ន្ធគ្រប់គ្រងអ្នកប្រើប្រាស់',
        'ShowInformation' => 'បង្ហាញព័ត៌មានទាំងអស់',
        'ShowInformationUser' => 'ព័ត៌មានអំពីអ្នកប្រើប្រាស់',
        'postUser' => 'ទំព័របញ្ចូលព័ត៌មានអ្នកប្រើប្រាស់',
        'addButtonUser' => '+បន្ថែម',
        'cancel' => 'តើអ្នចង់លុបដែរឬទេ?',
        'addUser' => 'បន្ថែមអ្នកប្រើប្រាស់ថ្មី',
        'whenyoudelete' => 'នៅពេលអ្នកលុប អ្នកមិនអាចទាញឯកសារបានមកវិញទេ!',
        'back'  => 'ចាកចេញ',
        'remove'  => 'លុប',
        'success' => 'ឯកសាររបស់លោកអ្នកលុបបានជោគជ័យ!',
        'cancel-delete' => 'អ្នកមិនបានលុបឯកសារទេ!',
        'id' => 'ល.រ',
        'name' => 'ឈ្មោះអ្នកប្រើប្រាស់',
        'email' => 'អុីមែល',
        'phone' => 'លេខទូរស័ព្ទ',
        'profile' => 'រូបភាព',
        'action' => 'សកម្មភាព',
        'search' => 'ស្វែងរក',
        'startdate' => 'ថ្ងៃចាប់ផ្តើម',
        'enddate' => 'ថ្ងៃបញ្ចប់',
        'selectoption' => 'ស្វែងរកតាមរយៈ',
        'showing' => 'ព័ត៌មានដែលបង្ហាញចំនួន',
        '0' => '០',
        '1' => '១',
        '2' => '២',
        '3' => '៣',
        '4' => '៤',
        '5' => '៥',
        '6' => '៦',
        '7' => '៧',
        '8' => '៨',
        '9' => '៩',
        'name_required' => 'សូមបញ្ចូលឈ្មោះផង ប្រូ&សុីស',
        'name_max' => 'មិនអាចបញ្ចូលអក្សរបានលើសពី៥តួឡើយ!',
        'email_required' => 'សូមបញ្ចូលអ៊ីមែលផង ប្រូ&សុីស',
        'email_unique' => 'ទិន្នន័យនេះធ្លាប់ត្រូវបានបញ្ចូលហើយ!',
        'phone_required' => 'សូមបញ្ចូលលេខទូរស័ព្ទ ប្រូ&សុីស',
        'phone_unique' => 'លេខទូរស័ព្ទនេះធ្លាប់ត្រូវបានបញ្ចូលហើយ!',
        'login' => 'ចូលកម្មវិធី',
        'logout' => 'ចាកចេញកម្មវិធី',
    ];
?>