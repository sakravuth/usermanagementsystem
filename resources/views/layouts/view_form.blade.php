@extends('index')
@section('information')    
        <section class="signup">
            <div class="container">
                <div class="col-lg-5">
                        <table class="table">
                            <a href="/information" class="glyphicon glyphicon-circle-arrow-left" style="font-size:30px;"></a>
                            <h2 class="form-title">Show <b>Information</b></h2>
                            <tbody>
                                <tr class="danger">
                                <td>Name</td>
                                <td><b>{{$users->name}}</b></td>
                                </tr>      
                                <tr class="">
                                <td>Email</td>
                                <td><b>{{$users->email}}</b></td>
                                </tr>
                                <tr class="success">
                                    <td>Phone</td>
                                    <td><b>{{$users->phone_number}}</b></td>
                                    </tr> 
                            </tbody>
                        </table>
                </div>
                <div class="col-lg-3">
                    <img src="/storage/{{$users->profile}}" style="width:200px; height:200px;">
                </div>
            </div>
            

        
@endsection