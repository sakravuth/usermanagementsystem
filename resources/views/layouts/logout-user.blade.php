@extends('index')

@section('logout-user')
    <div class="row">
            <div class="container col-lg-6" style="padding: 0px; overflow:hidden;">
                <h1 style="padding-top: 2%; padding-bottom:15px;">Logout All Users</h1>
                <form action="/logoutAction" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group"> <!-- Password !-->
                        <label for="name" class="control-label">Re-Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                        
                    </div>
                    
                    <div class="form-group"> <!-- Submit button !-->
                        <button type="submit" class="btn btn-primary">LogOutAll</button>
                    </div>	
                    
                </form>
            </div>
    </div>
@endsection