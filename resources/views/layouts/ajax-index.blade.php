@extends('index')



@section('view-ajax')
    <div class="container-fluid">
        <table class="table">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>PhoneMobile</td>
                    <td>Profile</td>
                </tr>
            </thead>

            <tbody>

            </tbody> <img src="">
        </table> 
        <input type="button" value="Get Data" id="getdata" class="btn btn-danger">
    </div>

    <script>
        $(document).ready(function(){
            $('#getdata').click(function(){
                $.ajax({
                    url: "/api/users",
                    method: "GET",
                    success: function(users){
                        userData = users.data;
                        userElement = "";
                        for(i=0; i<userData.length; i++){
                            userElement += "<tr>";
                                userElement += "<td>"+userData[i].id+"</td>";
                                userElement += "<td>"+userData[i].name+"</td>";
                                userElement += "<td>"+userData[i].email+"</td>";
                                userElement += "<td>"+userData[i].phone_number+"</td>";
                                //userElement += "<td>"+userData[i].profile+"</td>";
                                userElement += "<td>"+"<img src="+'/storage/'+userData[i].profile+" width=50px;height=50px;>"+"</td>";
                            userElement += "</tr>";
                        }
                        $("tbody").html(userElement)
                    }
                })
            })
        })
    </script>
@endsection