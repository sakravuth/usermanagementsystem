@extends('index')

@section('content')
    <div class="container row" style="margin-left:300px;">
        <div class="col-lg-5">
            <h1 style="padding-top: 3%;">Edit User Management</h1>
            
            <form action="{{route('article.edit', $user->id)}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
            
                    <div class="form-group"> <!-- Email field !-->
                        <label for="title" class="control-label">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
        
                        <label for="email" class="control-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}">
                        
                        <label for="phone_number" class="control-label">Phone</label>
                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{$user->phone_number}}">
                        
                    </div>
                    <div class="form-group">
                        <input type="file" id="profile" name="profile" class="form-control" value="{{$user->profile}}">
                    </div>
                    
                    <div class="form-group"> <!-- Submit button !-->
                        <button type="submit" class="btn btn-primary">Edit User</button>
                    </div>
                
            </form>
        </div>

        <div class="col-lg-4" style="margin-top: 60px;">
            <img src="/storage/{{$user->profile}}" style="width:200px; height:200px;">
        </div>
    </div>
@endsection