@extends('index')

@section('content')
    <div class="row">
            <div class="container col-lg-6" style="padding: 0px; overflow:hidden;">
                <h1 style="padding-top: 2%; padding-bottom:15px;">@lang('message.postUser')</h1>
                <form action="/add" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group"> <!-- Email field !-->
                        <label for="name" class="control-label">@lang('message.name')
                            @if($errors->has('name'))
                                <div class="error">{{$errors->first('name')}}</div>
                            @endif
                        
                        </label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">

                        <label for="email" class="control-label">@lang('message.email')</label>
                            @if($errors->has('email'))
                                <div class="error">{{$errors->first('email')}}</div>
                            @endif
                        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">

                        <label for="phone" class="control-label">@lang('message.phone')</label>
                            @if($errors->has('phone_number'))
                                <div class="error">{{$errors->first('phone_number')}}</div>
                            @endif
                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{old('phone_number')}}">

                        <label for=""></label>
                        <div class="form-group">
                            <input type="file" id="profile" name="profile" class="form-control">
                        </div>
                        
                    </div>
                    
                    <div class="form-group"> <!-- Submit button !-->
                        <button type="submit" class="btn btn-primary">@lang('message.addButtonUser')</button>
                    </div>	
                    
                </form>
            </div>
    </div>
@endsection