@extends('index')
@section('information')    
@hasrole(["admin"])
    <h1>This is Admin</h1>
@endhasrole
<div class="container">
        <div class="table-wrapper">
            <form action="/search" method="GET" autocomplete="off">
                @csrf
                <div class="table-title">
                    <div class="row">
                        <h2 style="font-size:30px;">@lang('message.ShowInformationUser')</h2>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                    <section class="search-sec" style="overflow:hidden;padding:0;margin-top:10px;">
                                            <div class="col-lg-12 container">
                                                <form action="#" method="POST" novalidate="novalidate">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="row">
                                                                <div class="col-lg-5">
                                                                    <label>@lang('message.search')</label>
                                                                    <input type="text" name="search" placeholder="Search anything!" class="form-control" value="{{isset($search)?$search:''}}">
                                                                </div>
                
                                                                
                                                                <div class="col-lg-2">
                                                                    <label>@lang('message.startdate')</label>
                                                                    <div id="datepicker" class="input-group date" name="startdate">
                                                                        <input class="form-control" type="text" readonly />
                                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <script>
                                                                    $(function () {
                                                                    $("#datepicker").datepicker({
                                                                        autoclose: true, 
                                                                        todayHighlight: true
                                                                      }).datepicker('update', new Date());
                                                                    });
                                                                </script>

                                                                <div class="col-lg-2">
                                                                        <label>@lang('message.enddate')</label>
                                                                        <div id="datepicker-end" class="input-group date" name="enddate">
                                                                            <input class="form-control" type="text" readonly />
                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <script>
                                                                        $(function () {
                                                                        $("#datepicker-end").datepicker({
                                                                            autoclose: true, 
                                                                            todayHighlight: true
                                                                        }).datepicker('update', new Date());
                                                                        });
                                                                    </script>
                                                                  
                                                                  <div class="col-lg-2">
                                                                    <label>@lang('message.selectoption')</label>
                                                                    <select name="name" class="form-control search-slt">
                                                                        @foreach ($filters as $f)
                                                                            <option value="{{$f}}">{{$f}}</option>
                                                                        @endforeach
                                                                        
                                                                    </select>
                                                                </div>

                                                                <div>
                                                                    <button type="submit" class="btn btn-primary" style="font-size:30%; margin-top:25px;"><i class="fa fa-search"></i></button>
                                                                </div>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </section>
                            </div>
                    </div>
                </div>
            </form>
            <table class="table table-striped table-hover" style="border: 1px solid lightgray;">
                <thead>
                    <tr style="height:50px;font-size: 20px;">
                        <th style="font-size:17px;">&nbsp;@lang('message.id')</th>
                        <th style="font-size:17px;">@lang('message.name')</th>
						<th style="font-size:17px;">@lang('message.email')</th>
                        <th style="font-size:17px;">@lang('message.phone')</th>
                        <th style="font-size:17px;">@lang('message.profile')</th>
                        <th style="font-size:17px;">@lang('message.action')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone_number}}</td>
                            <td><img src="/storage/{{$user->profile}}" style="height:50px;"></td>
                            <td style="width: 140px;">
                                <a href="{{route('view',$user->id)}}" class="btn btn-success btn-xs" style="width:30px;"><i class="fa fa-search-plus" style="color:white; padding:0;margin:0; font-size:22px;"></i></a>
                                <a href="{{route('article.view',$user->id)}}" class="btn btn-primary btn-xs" style="width:30px;"><i class="fa fa-edit" style="color:white; padding:0;margin:0; font-size:22px;"></i></a>
                                <a href="#" onclick="btndelete({{$user->id}})" class="btn btn-danger btn-xs" style="width:30px;"><i class="fa fa-trash" style="color:white; padding:0;margin:0; font-size:22px;"></i></a>
                            </td>
                        </tr>
                    @endforeach	
                </tbody>
            </table>

			<div class="clearfix" style="margin-top:1%;">
            <div class="hint-text">@lang('message.showing') <b>{{$users->count()}}</div>
                {{$users->appends(request()->all())->links()}}
            </div>
        </div>
    </div>
    <script>
        function btndelete(id){
            swal({
                title: "@lang('message.cancel')",
                text: "@lang('message.whenyoudelete')",
                icon: "warning",
                dangerMode: true,
                buttons: ['@lang('message.back')', '@lang('message.remove')'],
                })
                .then((willDelete) => {
                if (willDelete) {
                    window.location.href="/delete/"+id;
                    swal({
                        title: "@lang('message.success')",
                        icon: "success",
                        buttons: false,
                    });
                }
                });
            }
    </script>
@endsection